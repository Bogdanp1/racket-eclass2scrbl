# Eclass2Scrbl

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-eclass2scrbl">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-eclass2scrbl/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-eclass2scrbl/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-eclass2scrbl/badges/master/pipeline.svg">
    </a>
</p>

Convert Gentoo Eclasses to Scribble documents.


## About

`eclass2scrbl` is a Racket tool to convert Gentoo Eclass documents
into Scribble documents.

A Scribble document can be converted to a number of formats:
HTML, Markdown, PDF, Tex, etc.


## Online Documentation

You can read the documentation online on the
[GitLab pages](https://gentoo-racket.gitlab.io/racket-eclass2scrbl/).


## License

Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
