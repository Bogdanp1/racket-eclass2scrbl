#!/bin/sh


# This file is part of racket-eclass2scrbl - Eclass to Scribble converter.
# Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# racket-eclass2scrbl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# racket-eclass2scrbl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-eclass2scrbl.  If not, see <https://www.gnu.org/licenses/>.


set -e
trap 'exit 128' INT
export PATH


root_source="$(realpath "$(dirname "${0}")/../")"
root_cache="${root_source}"/.cache
root_scripts="${root_source}"/scripts
gentoo_scrbl="${root_cache}"/gentoo.scrbl

if command -v portageq >/dev/null 2>&1 ; then
    gentoo_repo_path="$(portageq get_repo_path / gentoo)"
else
    gentoo_repo_path="$(pwd)"
fi


"${root_scripts}"/run.sh --version

mkdir -p "${root_cache}"

cat > "${gentoo_scrbl}" << EOF
#lang scribble/manual

@title[#:tag "eclasses"]{Eclasses}

@table-of-contents[]

EOF


for eclass_file in "${gentoo_repo_path}"/eclass/*.eclass ; do
    eclass_name="$(basename "$(basename "${eclass_file}")" .eclass)"
    eclass_scrbl="${root_cache}"/"${eclass_name}".scrbl

    "${root_scripts}"/run.sh --output "${eclass_scrbl}" "${eclass_file}"

    echo "@include-section{${eclass_name}.scrbl}" >> "${gentoo_scrbl}"
done
